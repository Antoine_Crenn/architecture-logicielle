﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using VCubWatcher.Models;

namespace VCubWatcher.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Liste_Stations()
        {
            var stations = Trouver_Station_Velo();
            return View(stations);
        }

        public IActionResult Mes_Favoris()
        {
            return View();
        }

        public IActionResult Carte()
        {
            var stations = Trouver_Station_Velo();
            return View(stations);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private static readonly HttpClient client = new HttpClient();

        public static List<Station_Velo> Trouver_Station_Velo()
        {
            var url = client.GetStringAsync("https://api.alexandredubois.com/vcub-backend/vcub.php");
            var donnee_json = url.Result;
            var resultat = JsonConvert.DeserializeObject<List<Station_Velo>>(donnee_json);
            var resultat_trier = resultat.OrderBy(Station_Velo => Station_Velo.Nom).ToList();
            return resultat_trier;
        }
    }
}
