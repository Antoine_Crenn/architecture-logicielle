﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VCubWatcher.Models
{
    public class Station_Velo
    {
        [JsonProperty("id")]
        public int Identifiant { get; set; }

        [JsonProperty("name")]
        public string Nom { get; set; }

        [JsonProperty("bike_count")]
        public int Nombre_Velo { get; set; }

        [JsonProperty("electric_bike_count")]
        public int Nombre_Velo_Electrique { get; set; }

        [JsonProperty("bike_count_total")]
        public int Nombre_Velo_Total { get; set; }

        [JsonProperty("slot_count")]
        public int Nombre_Emplacement_Libre { get; set; }

        [JsonProperty("is_online")]
        public bool Fonctionnement { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }
    }
}
